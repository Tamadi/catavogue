<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/collections/create.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<h3>Create Collection</h3>
<?php echo Form::open(['action' => 'CollectionsController@store','method' => 'POST', 'enctype' => 'multipart/form-data']); ?><br><br>
    <?php echo e(Form::text('name','',['placeholder' => 'Collection Name'])); ?><br><br>
    <?php echo e(Form::textarea('description','',['placeholder' => 'Collection Description'])); ?><br><br>
    <?php echo e(Form::file('cover_image')); ?><br><br>
    <?php echo e(Form::submit('submit', ['class' =>'btn btn-danger'])); ?><br><br>
  <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>