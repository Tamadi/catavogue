<?php /* /Users/ToluAmadi/Sites/laravel/resources/views//dashboard.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    You are logged in!
                </div>
                    <div class="mt-3 panel-header">
                        <h4> Your Collections</h4></div>
                    <div class="panel-body">
                        <?php if(count($collections)): ?>
                          <table class="table table-striped">
                            <tr>
                              <th>Collections</th>
                              <th><?php echo e($collection->name); ?></th>
                              <th><?php echo e($collection->cover_image); ?></th>
                            </tr>
                            <?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <tr>
                                <td><br><br></td>
                              </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </table>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>