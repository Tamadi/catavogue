<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/photos/create.blade.php */ ?>
<?php $__env->startSection('content'); ?>
  <h3>Upload Photo</h3>
  <?php echo Form::open(['action' => 'PhotosController@store','method' => 'POST', 'enctype' => 'multipart/form-data']); ?><br><br>
    <?php echo e(Form::text('title','',['placeholder' => 'Photo Title'])); ?><br><br>
    <?php echo e(Form::textarea('description','',['placeholder' => 'Photo Description'])); ?><br><br>
    <?php echo e(Form::text('price','',['placeholder' => 'Price'])); ?><br><br>
    <?php echo e(Form::text('url','',['placeholder' => 'Buy'])); ?><br><br>
    <?php echo e(Form::hidden('collection_id', $collection_id)); ?>

    <?php echo e(Form::file('photo')); ?><br><br>
    <?php echo e(Form::submit('submit', ['class' => 'btn btn-danger'])); ?><br><br>
  <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>