<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/collections/index.blade.php */ ?>
<?php $__env->startSection('content'); ?>
	<?php if(count($collections) > 0): ?>
    	<?php
      		$colcount = count($collections);
  	 		 $i = 1;
    	?>
		<div class="album container">
		  <div class="row text-center">
		  	<?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          		<?php if($i == $colcount): ?>	
			    	<div class="col-sm media">
			      		<a href="/collections/<?php echo e($collection->id); ?>">
			  				<img class="img-thumbnail align-self-start mr-3" src="storage/collection_covers/<?php echo e($collection->cover_image); ?>" alt="<?php echo e($collection->name); ?>">
			  			</a>
			  			<hr>
			 				<div class="media-body">
			   	 				<h5 class="mt-0"><?php echo e($collection->name); ?></h5>
			   	 				<hr>
			    					<p><?php echo e($collection->description); ?></p><hr>
			   		 		</div>
			   		</div>
		<?php else: ?>
            <div class="col-sm media">
              <a href="/collections/<?php echo e($collection->id); ?>">
                <img class="img-thumbnail align-self-start mr-3" src="storage/collection_covers/<?php echo e($collection->cover_image); ?>" alt="<?php echo e($collection->name); ?>">
              </a>
              <hr>
              <h5 class="mt-0"><?php echo e($collection->name); ?></h5>
              <hr>
          <?php endif; ?>
        	<?php if($i % 3 == 0): ?>
          </div></div><div class="row text-center">
        	<?php else: ?>
            </div>
          <?php endif; ?>
        	<?php $i++; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  <?php else: ?>
    <p>No Collections To Display</p>
  <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>