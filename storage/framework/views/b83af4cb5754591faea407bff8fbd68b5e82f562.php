<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/photos/show.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<div class="container">
	<a href="/collections/<?php echo e($photo->collection_id); ?>">Back To Collection</a>
  	<hr>
  	<table class="table table-striped">
    <tr>
  	<img class="w-50" src="/storage/photos/<?php echo e($photo->collection_id); ?>/<?php echo e($photo->photo); ?>" alt="<?php echo e($photo->title); ?>">
  			<th><?php echo e($photo->title); ?></th>
         	<th><?php echo e($photo->description); ?></th>
         	<th>$<?php echo e($photo->price); ?></th>
         	<th></th>
        </tr>
         <tr>
            <td><a class="btn btn-info" href="<?php echo e($photo->url); ?>">Buy Now!</a></td>
          </tr>
 
  		<p></p>
  	<br><br>
	  <?php echo Form::open(['action' => ['PhotosController@destroy', $photo->id], 'method' => 'POST']); ?>

	    <?php echo e(Form::hidden('_method', 'DELETE')); ?>

	    <?php echo e(Form::submit('Delete Photo', ['class' => 'btn btn-danger'])); ?>

	  <?php echo Form::close(); ?>

	  <hr>
  	<small>Size: <?php echo e($photo->size); ?></small>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>