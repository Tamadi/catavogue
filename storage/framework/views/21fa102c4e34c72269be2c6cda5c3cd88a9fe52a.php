<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/collections/show.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<div class="container">
	<h1 class="mt-3 mr-3"><?php echo e($collection->name); ?> </h1>
	<a class="btn btn-info" href="/mycollections">Previous</a>
	<a class="btn btn-danger" href="/photos/create/<?php echo e($collection->id); ?>">Upload Photos To Collection</a>
	<hr>
</div>
<?php if(count($collection->photos) > 0): ?>
    	<?php
      		$colcount = count($collection->photos);
  	 		 $i = 1;
    	?>
		<div class="photos container">
		  <div class="row text-center">
		  	<?php $__currentLoopData = $collection->photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          		<?php if($i == $colcount): ?>	
			    	<div class="col-sm media">
			      		<a href="/photos/<?php echo e($photo->id); ?>">
			  				<img class="img-thumbnail align-self-start mr-3" src="/storage/photos/<?php echo e($photo->collection_id); ?>/<?php echo e($photo->photo); ?>" alt="<?php echo e($photo->title); ?>">
			  			</a>
			  			<hr>
			 				<div class="media-body">
			   	 				<h5 class="mt-0"><?php echo e($photo->title); ?></h5>
			   	 				<hr>
			    					<p><?php echo e($photo->description); ?></p><hr>
			   		 		</div>
			   		</div>
		<?php else: ?>
            <div class="col-sm media">
              <a href="/photos/<?php echo e($photo->id); ?>">
                <img class="img-thumbnail align-self-start mr-3" src="/storage/photos/<?php echo e($photo->collection_id); ?>/<?php echo e($photo->photo); ?>" alt="<?php echo e($photo->title); ?>">
              </a>
              <hr>
              <h5 class="mt-0"><?php echo e($photo->title); ?></h5>
              <hr>
          <?php endif; ?>
        	<?php if($i % 3 == 0): ?>
          </div></div><div class="row text-center">
        	<?php else: ?>
            </div>
          <?php endif; ?>
        	<?php $i++; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  <?php else: ?>
    <p>No Photos To Display</p>
  <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>