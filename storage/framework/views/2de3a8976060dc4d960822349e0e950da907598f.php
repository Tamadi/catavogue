<?php /* /Users/ToluAmadi/Sites/laravel/resources/views/inc/topbar.blade.php */ ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">CataVogue</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/mycollections">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/collections/create">Create Collection</a>
      <!-- <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a> -->
    </div>
    <div class="navbar-nav navbar-left">
      <?php if(auth()->guard()->guest()): ?>
        <a class="nav-item nav-link navbar-right" href="<?php echo e(url('/login')); ?>">Login</a>

        <a class="btn btn-success navbar-btn navbar-right" href="<?php echo e(url('/register')); ?>">Register</a>
          <?php else: ?>        
        <a class="nav-item nav-link navbar-right navbar-btn"href="<?php echo e(url('/logout')); ?>">logout</a>        
      <?php endif; ?>
    </div>
  </div>
</nav>