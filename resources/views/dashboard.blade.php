@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                    <div class="mt-3 panel-header">
                        <h4> Your Collections</h4></div>
                    <div class="panel-body">
                        @if(count($collections))
                          <table class="table table-striped">
                            <tr>
                              <th>Collections</th>
                              <th>{{$collection->name}}</th>
                              <th>{{$collection->cover_image}}</th>
                            </tr>
                            @foreach($collections as $collection)
                              <tr>
                                <td><br><br></td>
                              </tr>
                            @endforeach
                          </table>
                        @endif
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
