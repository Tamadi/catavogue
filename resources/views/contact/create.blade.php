@extends('layouts.app')

@section('content')
  <h3>Contact Collection Owner</h3>
  {!!Form::open(['action' => 'ContactController@store','method' => 'POST', 'enctype' => 'multipart/form-data'])!!}<br><br>
    {{Form::text('title','',['placeholder' => 'Photo Title'])}}<br><br>
    {{Form::text('link','',['placeholder' => 'Photo Title'])}}<br><br>
    {{Form::textarea('message','',['placeholder' => 'Message'])}}<br><br>
    {{Form::hidden('collection_id', $collection_id)}}
    {{Form::file('photo')}}<br><br>
    {{Form::submit('submit')}}<br><br>
  {!! Form::close() !!}
@endsection