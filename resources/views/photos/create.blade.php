@extends('layouts.app')

@section('content')
  <h3>Upload Photo</h3>
  {!!Form::open(['action' => 'PhotosController@store','method' => 'POST', 'enctype' => 'multipart/form-data'])!!}<br><br>
    {{Form::text('title','',['placeholder' => 'Photo Title'])}}<br><br>
    {{Form::textarea('description','',['placeholder' => 'Photo Description'])}}<br><br>
    {{Form::text('price','',['placeholder' => 'Price'])}}<br><br>
    {{Form::text('url','',['placeholder' => 'Buy'])}}<br><br>
    {{Form::hidden('collection_id', $collection_id)}}
    {{Form::file('photo')}}<br><br>
    {{Form::submit('submit', ['class' => 'btn btn-danger'])}}<br><br>
  {!! Form::close() !!}
@endsection