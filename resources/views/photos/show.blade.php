@extends('layouts.app')

@section('content')
<div class="container">
	<a href="/collections/{{$photo->collection_id}}">Back To Collection</a>
  	<hr>
  	<table class="table table-striped">
    <tr>
  	<img class="w-50" src="/storage/photos/{{$photo->collection_id}}/{{$photo->photo}}" alt="{{$photo->title}}">
  			<th>{{$photo->title}}</th>
         	<th>{{$photo->description}}</th>
         	<th>${{$photo->price}}</th>
         	<th></th>
        </tr>
         <tr>
            <td><a class="btn btn-info" href="{{$photo->url}}">Buy Now!</a></td>
          </tr>
 
  		<p></p>
  	<br><br>
	  {!!Form::open(['action' => ['PhotosController@destroy', $photo->id], 'method' => 'POST'])!!}
	    {{Form::hidden('_method', 'DELETE')}}
	    {{Form::submit('Delete Photo', ['class' => 'btn btn-danger'])}}
	  {!!Form::close()!!}
	  <hr>
  	<small>Size: {{$photo->size}}</small>
  </div>
@endsection