@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="mt-3 mr-3">{{$collection->name}} </h1>
	<a class="btn btn-info" href="/mycollections">Previous</a>
	<a class="btn btn-danger" href="/photos/create/{{$collection->id}}">Upload Photos To Collection</a>
	<hr>
</div>
@if(count($collection->photos) > 0)
    	<?php
      		$colcount = count($collection->photos);
  	 		 $i = 1;
    	?>
		<div class="photos container">
		  <div class="row text-center">
		  	@foreach($collection->photos as $photo)
          		@if($i == $colcount)	
			    	<div class="col-sm media">
			      		<a href="/photos/{{$photo->id}}">
			  				<img class="img-thumbnail align-self-start mr-3" src="/storage/photos/{{$photo->collection_id}}/{{$photo->photo}}" alt="{{$photo->title}}">
			  			</a>
			  			<hr>
			 				<div class="media-body">
			   	 				<h5 class="mt-0">{{$photo->title}}</h5>
			   	 				<hr>
			    					<p>{{$photo->description}}</p><hr>
			   		 		</div>
			   		</div>
		@else
            <div class="col-sm media">
              <a href="/photos/{{$photo->id}}">
                <img class="img-thumbnail align-self-start mr-3" src="/storage/photos/{{$photo->collection_id}}/{{$photo->photo}}" alt="{{$photo->title}}">
              </a>
              <hr>
              <h5 class="mt-0">{{$photo->title}}</h5>
              <hr>
          @endif
        	@if($i % 3 == 0)
          </div></div><div class="row text-center">
        	@else
            </div>
          @endif
        	<?php $i++; ?>
        @endforeach
      </div>
    </div>
  @else
    <p>No Photos To Display</p>
  @endif
@endsection