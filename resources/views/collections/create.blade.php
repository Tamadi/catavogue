@extends('layouts.app')

@section('content')
<h3>Create Collection</h3>
{!!Form::open(['action' => 'CollectionsController@store','method' => 'POST', 'enctype' => 'multipart/form-data'])!!}<br><br>
    {{Form::text('name','',['placeholder' => 'Collection Name'])}}<br><br>
    {{Form::textarea('description','',['placeholder' => 'Collection Description'])}}<br><br>
    {{Form::file('cover_image')}}<br><br>
    {{Form::submit('submit', ['class' =>'btn btn-danger'])}}<br><br>
  {!! Form::close() !!}
@endsection