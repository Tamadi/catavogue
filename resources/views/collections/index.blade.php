@extends('layouts.app')

@section('content')
	@if(count($collections) > 0)
    	<?php
      		$colcount = count($collections);
  	 		 $i = 1;
    	?>
		<div class="album container">
		  <div class="row text-center">
		  	@foreach($collections as $collection)
          		@if($i == $colcount)	
			    	<div class="col-sm media">
			      		<a href="/collections/{{$collection->id}}">
			  				<img class="img-thumbnail align-self-start mr-3" src="storage/collection_covers/{{$collection->cover_image}}" alt="{{$collection->name}}">
			  			</a>
			  			<hr>
			 				<div class="media-body">
			   	 				<h5 class="mt-0">{{$collection->name}}</h5>
			   	 				<hr>
			    					<p>{{$collection->description}}</p><hr>
			   		 		</div>
			   		</div>
		@else
            <div class="col-sm media">
              <a href="/collections/{{$collection->id}}">
                <img class="img-thumbnail align-self-start mr-3" src="storage/collection_covers/{{$collection->cover_image}}" alt="{{$collection->name}}">
              </a>
              <hr>
              <h5 class="mt-0">{{$collection->name}}</h5>
              <hr>
          @endif
        	@if($i % 3 == 0)
          </div></div><div class="row text-center">
        	@else
            </div>
          @endif
        	<?php $i++; ?>
        @endforeach
      </div>
    </div>
  @else
    <p>No Collections To Display</p>
  @endif

@endsection