<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/mycollections', "CollectionsController@index");
Route::get('/collections', "CollectionsController@index");
Route::get('/collections/create', "CollectionsController@create");
Route::get('/collections/{id}', "CollectionsController@show");
Route::post('/collections/store', "CollectionsController@store");

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'PhotosController@index')->name('landing');
Route::get('/photos/create/{id}', "PhotosController@create");
Route::post('/photos/store', 'PhotosController@store');
Route::get('/photos/{id}', "PhotosController@show");
Route::delete('/photos/{id}', "PhotosController@destroy");
Auth::routes();
Route::resource('profiles', 'ProfilesController');

// Route::get('/dashboard', 'HomeController@index')->name('dashboard');
