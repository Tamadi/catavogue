<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
	Protected $fillable = ['name','description', 'cover_image' ];
    public function photos(){
    	return $this->hasMany('App\Photo');
    }
    protected $primaryKey = 'id';

    // User Collections
    public function user(){
    	return $this->belongsTo('App\User');
    }
}
