<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Collection;
use App\User;
use App\Photo;
// use Auth;

class CollectionsController extends Controller
{
    public function index(){
        $collections = Collection::with('Photos')->get();
    	return view('collections.index')->with('collections', $collections);
    }
    public function create(){
    	return view('collections.create');
    }
    public function store(Request $request){
    	$this->validate($request, [
    		'name'=> 'required',
    		'cover_image'=>'image|max:1999',
    	]);
    	// Get folename with extension
    	$filenameWithExt = $request->file('cover_image')->getClientOriginalName(); 
    	// Get just the filename
    	$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME); 
    	//Get extension
    	$extension = $request ->file('cover_image')->getClientOriginalExtension();
    	//Create new filename
    	$filenameToStore = $filename.'_'.time(). '.'.$extension;
    	//Upload image
    	$path = $request->file('cover_image')->storeAs('public/collection_covers', $filenameToStore);
    	//Create collection
        $collection = new Collection;
        $collection->name = $request->input('name');
        $collection->description = $request->input('description'); 
        $collection->cover_image = $filenameToStore;

        $collection->save();
        return redirect('/collections')->with('success', 'Collection Catalogue Created');
    }
    public function show($id){
        $collection = Collection::with('Photos')->find($id);
        return view('collections.show')->with('collection', $collection);
    }

}

