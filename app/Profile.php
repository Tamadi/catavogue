<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = array('name', 'address', 'email', 'phone', 'website', 'bio', 'paypal');
   
}
