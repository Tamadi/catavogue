<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['collection_id', 'description', 'photo', 'title', 'size', 'price','url'];
    
    public function collection (){
    	return $this->belongsTo('App\Collection');
    }
}
